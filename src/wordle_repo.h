#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#include <fstream>
#include <span>
#include <vector>

#include <iostream>

#include "wordle.h"

template <size_t N>
struct WordleRepo final {
    using Index = std::uint16_t;

    [[nodiscard]] explicit WordleRepo(char const* path) {
        auto input = std::ifstream{path};
        for (auto wordle = Wordle<N>{}; (input >> wordle).ignore().good(); data.emplace_back(wordle)) {}
        std::cout << size() << '\n';
    }

    [[nodiscard]] Wordle<N> operator[](std::uint16_t idx) const { return data[idx]; }

    [[nodiscard]] constexpr size_t size() const noexcept { return data.size(); }

private:
    std::vector<Wordle<N>> data{};
};

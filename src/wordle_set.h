#pragma once

#include <algorithm>
#include <concepts>
#include <memory>
#include <numeric>
#include <ostream>
#include <queue>
#include <vector>

#include "wordle_repo.h"

template <size_t N, typename Allocator = std::allocator<Wordle<N>>>
struct WordleSet
{
    explicit WordleSet(WordleRepo<N> const &repo, Allocator a = {}) : data(repo.size(), static_cast<DataAllocatorType>(a)), repo{std::addressof(repo)}
    {
        using std::begin, std::end;
        std::iota(begin(data), end(data), typename WordleRepo<N>::Index{0});
    }

    [[nodiscard]] size_t size() const noexcept
    {
        return data.size();
    }

    [[nodiscard]] bool empty() const noexcept
    {
        return data.empty();
    }

    template <typename F>
    [[nodiscard]] WordleSet filtered(F &&f) const
    {
        auto result = WordleSet(data.get_allocator());
        result.repo = repo;
        std::ranges::copy_if(data, std::back_inserter(result.data), std::forward<F>(f), [this](auto idx)
                             { return (*repo)[idx]; });
        return result;
    }

    friend std::ostream &operator<<(std::ostream &out, const WordleSet &wordleSet)
    {
        out << "WordleSet<" << N << ">{\n";
        std::ranges::for_each(wordleSet.data, [&out, &repo = *wordleSet.repo](auto idx)
                              { out << '\t' << repo[idx] << '\n'; });
        return out << '}';
    }

    friend WordleSet<N, Allocator> intersection<>(std::span<WordleSet<N, Allocator>> sets, Allocator allocator);

private:
    using DataType = typename WordleRepo<N>::Index;
    using DataAllocatorType = typename std::allocator_traits<Allocator>::rebind_alloc<DataType>;
    using Data = std::vector<DataType, DataAllocatorType>;

    Data data;

    WordleRepo<N> const *repo{};

    WordleSet(Allocator a) : data{a}
    {
        assert(std::ranges::is_sorted(data));
    }
};

template <size_t N, typename Allocator>
WordleSet<N, Allocator> intersection(std::span<WordleSet<N, Allocator>> sets, Allocator allocator = {})
{
    if (std::ranges::empty(sets))
    {
        return {std::move(allocator)};
    }

    static constexpr auto cmp = [](const auto &lhs, const auto &rhs)
    {
        if (lhs.empty())
        {
            return false;
        }
        if (rhs.empty())
        {
            return true;
        }
        return rhs.front() < lhs.front();
    };
    auto ranges = [&]
    {
        auto ranges = std::vector<std::span<const typename WordleRepo<N>::Index>>(sets.size());
        std::ranges::transform(sets, begin(ranges), [](const auto &wordleSet)
                               { return std::span{wordleSet.data}; });
        ranges.erase(end(std::ranges::remove_if(ranges, [](const auto &set)
                                                { return set.empty(); })),
                     end(ranges));
        return ranges;
    }();

    std::ranges::make_heap(ranges, cmp);

    auto result = WordleSet<N, Allocator>{std::move(allocator)};

    while (!std::ranges::empty(ranges.front()))
    {
        if (std::ranges::adjacent_find(ranges, std::ranges::not_equal_to{}, &std::span<const typename WordleRepo<N>::Index>::front) == end(ranges))
        {
            result.data.emplace_back(ranges.front().front());
        }
        std::ranges::pop_heap(ranges, cmp);
        ranges.back() = ranges.back().subspan(1);
        std::ranges::push_heap(ranges, cmp);
    }
    return result;
}

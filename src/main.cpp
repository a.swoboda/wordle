#include <iterator>
#include <string>
#include <unordered_set>

#include "constraint.h"
#include "wordle_repo.h"
#include "wordle_set.h"

int main(int argc, char const *argv[])
{
    if (argc < 3)
    {
        return 1;
    }
    const auto repo = WordleRepo<5>{argv[1]};
    const auto wordleSet = WordleSet{repo};

    std::cout << "There is a total of " << wordleSet.size() << " words.\n";

    const auto admissibleSet = [&]
    {
        auto admissibleStream = std::ifstream(argv[2]);
        const auto admissibleSet = std::unordered_set(std::istream_iterator<std::string>{admissibleStream}, std::istream_iterator<std::string>{});
        return wordleSet.filtered([&admissibleSet](auto wordle)
                                  { const auto w = std::string{wordle[0], wordle[1], wordle[2], wordle[3], wordle[4]};
                                                return admissibleSet.contains(w); });
    }();

    std::cout << admissibleSet.size() << " of these are admissible.\n";

    std::unordered_map<Constraint<5>, WordleSet<5>> constraintMap{};

    const auto constraintDomain = Constraint<5>::domain();
    for (const auto c : constraintDomain)
    {
        const auto constrainedSet = c(admissibleSet);
        if (!constrainedSet.empty())
        {
            constraintMap.try_emplace(c, c(wordleSet));
        }
    }

    std::cout << "From initially " << size(constraintDomain) << " constraints, there are "
              << size(constraintMap) << " constraint sets left.\n";
    const auto acc = std::accumulate(begin(constraintMap), end(constraintMap), size_t{}, [](auto acc, const auto &p)
                                     { return acc + p.second.size(); });
    std::cout << "There are on average " << acc / size(constraintMap) << " words per set.\n";
    std::cout << "The total mumber of wordles stored is " << acc << ".\n";
    auto testArray = std::vector{wordleSet, admissibleSet};
    const auto intersectionTest = intersection(std::span{testArray});
    std::cout << "Test: The intersection of the total and the admissible set has size " << intersectionTest.size() << '\n';
}

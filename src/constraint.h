#pragma once

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <functional>
#include <ranges>

#include "wordle_set.h"

enum class ResultColor : std::uint8_t
{
    GREEN,
    YELLOW,
    GRAY,
};

template <size_t N>
struct Constraint;

template <>
struct Constraint<5>
{
    static constexpr size_t N{5};

    Constraint() = default;

    template <typename Allocator = std::allocator<Constraint<N>>>
    static std::vector<Constraint<N>, Allocator> fromGuess(Wordle<N>::View guess, std::array<ResultColor, N> reply);

    template <typename Allocator>
    WordleSet<N, Allocator> operator()(WordleSet<N, Allocator> const &wordleSet) const;

    template <typename Allocator = std::allocator<Constraint<5>>>
    [[nodiscard]] static std::vector<Constraint<5>, Allocator> domain(Allocator allocator = {})
    {
        std::vector<Constraint<5>, Allocator> result(26 * 179, std::move(allocator));
        auto it = begin(result);
        for (auto c = 'a'; c != std::plus<char>{}('z', 1); ++c)
        {
            for (std::uint8_t t{}; t != 179; ++t, ++it)
            {
                *it = Constraint<5>{c, t};
            }
        }
        return result;
    }

    enum class GuessColor : std::uint8_t
    {
        GREEN,
        YELLOW,
        GRAY,
        OTHER,
    };

    auto operator<=>(const Constraint<5> &) const = default;

private:
    friend struct std::hash<Constraint<5>>;
    char c{};
    std::uint8_t t{};

    Constraint(char c, std::array<GuessColor, N> guess);

    Constraint(char c, std::uint8_t t) : c{c}, t{t} {}

    static GuessColor toGuess(ResultColor resultColor)
    {
        return static_cast<GuessColor>(resultColor);
    }
};

template <>
struct std::hash<Constraint<5>>
{
    size_t operator()(Constraint<5> c) const
    {
        return std::hash<size_t>{}(size_t{c.c} << 16 | c.t);
    }
};

// implementation

namespace details
{

    inline constexpr std::array<std::uint8_t, 19> offsets{1, 6, 16, 26, 36, 66, 76, 81, 82, 87, 92, 102, 112, 132, 142, 172, 177, 178};

    constexpr std::uint8_t map(std::uint8_t i, std::uint8_t j)
    {
        assert(i < 5);
        assert(j < 5);
        assert(i != j);
        constexpr auto lookup = std::array{
            std::array<std::uint8_t, 5>{255, 0, 1, 2, 3},
            std::array<std::uint8_t, 5>{0, 255, 4, 5, 6},
            std::array<std::uint8_t, 5>{1, 4, 255, 7, 8},
            std::array<std::uint8_t, 5>{2, 5, 7, 255, 9},
            std::array<std::uint8_t, 5>{3, 6, 8, 9, 255},
        };
        return lookup[i][j];
    }

    std::array<std::uint8_t, 2> map5over2(std::uint8_t code)
    {
        switch (code)
        {
        default:
            assert(!"invalid code");
            [[falltrough]];
        case 0:
            return {0, 1};
        case 1:
            return {0, 2};
        case 2:
            return {0, 3};
        case 3:
            return {0, 4};
        case 4:
            return {1, 2};
        case 5:
            return {1, 3};
        case 6:
            return {1, 4};
        case 7:
            return {2, 3};
        case 8:
            return {2, 4};
        case 9:
            return {3, 4};
        }
    }
}

template <typename Allocator>
inline WordleSet<5, Allocator> Constraint<5>::operator()(WordleSet<5, Allocator> const &wordleSet) const
{
    if (t == 0)
    { // 0 occurences
        return wordleSet.filtered([this](Wordle<N>::View wordle)
                                  { return std::ranges::find(wordle, c) == end(wordle); });
    }
    else if (t < details::offsets[1])
    { // exactly 1 occurrence, in place i
        return wordleSet.filtered([this, i = t - details::offsets[0]](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 1 and wordle[i] == c; });
    }
    else if (t < details::offsets[2])
    { // exactly 1 occurrence, not in places i, j
        const auto [i, j] = details::map5over2(t - details::offsets[1]);
        return wordleSet.filtered([this, i = i, j = j](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 1 and wordle[i] != c and wordle[j] != c; });
    }
    else if (t < details::offsets[3])
    { // exactly 1 occurrence, either in place i or j
        const auto [i, j] = details::map5over2(t - details::offsets[2]);
        return wordleSet.filtered([this, i = i, j = j](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 1 and (wordle[i] == c or wordle[j] == c); });
    }
    else if (t < details::offsets[4])
    { // exactly 2 occurrences, in places i, j
        const auto [i, j] = details::map5over2(t - details::offsets[3]);
        return wordleSet.filtered([this, i = i, j = j](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 2 and wordle[i] == c and wordle[j] == c; });
    }
    else if (t < details::offsets[5])
    { // exactly 2 occurrences, in place k, not in places i, j
        const auto code = t - details::offsets[4];
        const auto [i, j] = details::map5over2(code / 3);
        auto k = code % 3;
        k += k <= i;
        k += k <= j;
        return wordleSet.filtered([this, i = i, j = j, k = k](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 2 and wordle[k] == c and wordle[i] != c and wordle[j] != c; });
    }
    else if (t < details::offsets[6])
    { // exactly 3 occurrences, not in places i, j
        const auto [i, j] = details::map5over2(t - details::offsets[5]);
        return wordleSet.filtered([this, i = i, j = j](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 3 and wordle[i] != c and wordle[j] != c; });
    }
    else if (t < details::offsets[7])
    {
        return wordleSet.filtered([this, i = t - details::offsets[6]](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 4 and wordle[i] != c; });
    }
    else if (t < details::offsets[8])
    {
        return wordleSet.filtered([this](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 5; });
    }
    else if (t < details::offsets[9])
    { // at least 1 occurrence in position i
        const auto i = t - details::offsets[8];
        return wordleSet.filtered([this, i](Wordle<N>::View wordle)
                                  { return wordle[i] == c; });
    }
    else if (t < details::offsets[10])
    { // at least 1 occurrence, not in position i
        const auto i = t - details::offsets[9];
        return wordleSet.filtered([this, i](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) > 0 and wordle[i] != c; });
    }
    else if (t < details::offsets[11])
    { // at least 2 occurrences, in positions i, j
        const auto [i, j] = details::map5over2(t - details::offsets[10]);
        return wordleSet.filtered([this, i = i, j = j](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) > 1 and wordle[i] == c and wordle[j] == c; });
    }
    else if (t < details::offsets[12])
    { // at least 2 occurrences, neither in position i nor j
        const auto [i, j] = details::map5over2(t - details::offsets[11]);
        return wordleSet.filtered([this, i = i, j = j](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) > 1 and wordle[i] != c and wordle[j] != c; });
    }
    else if (t < details::offsets[13])
    {
        const auto code = t - details::offsets[12];
        const auto i = code / 4;
        const auto rem = code % 4;
        const auto j = rem + static_cast<std::uint8_t>(i <= j);
        return wordleSet.filtered([this, i = i, j = j](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) > 1 and wordle[i] == c and wordle[j] != c; });
    }
    else if (t < details::offsets[14])
    {
        const auto [i, j] = details::map5over2(t - details::offsets[13]);
        return wordleSet.filtered([this, i = i, j = j](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 3 + static_cast<std::uint8_t>(wordle[i] == c) + static_cast<std::uint8_t>(wordle[j] == c); });
    }
    else if (t < details::offsets[15])
    {
        const auto code = t - details::offsets[14];
        const auto [i, j] = details::map5over2(code / 3);
        auto k = code % 3;
        k += static_cast<std::uint8_t>(i <= k);
        k += static_cast<std::uint8_t>(j <= k);
        return wordleSet.filtered([this, i = i, j = j, k](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) > 3 and wordle[i] == c and wordle[j] == c and wordle[k] != c; });
    }
    else if (t < details::offsets[16])
    {
        const auto i = t - details::offsets[15];
        return wordleSet.filtered([this, i](Wordle<N>::View wordle)
                                  { return std::ranges::count(wordle, c) == 4 + static_cast<std::uint8_t>(wordle[i] == c); });
    }
    else if (t < details::offsets[17])
    {
        return wordleSet.filtered([this](Wordle<N>::View wordle)
                                  { return std::ranges::end(wordle) == std::ranges::find_if(wordle, [this](auto)
                                                                                            { return c != this->c; }); });
    }
    return wordleSet;
}

Constraint<5>::Constraint(char c, std::array<GuessColor, 5> guess) : c{c}, t{/*TODO*/}
{
    using std::end;
    static constexpr auto notGreen = [](auto c)
    { return c != GuessColor::GREEN; };
    if (const auto grayCount = std::ranges::count(guess, GuessColor::GRAY); grayCount != 0)
    { // we know the exact count
        t = [guess, grayCount]() -> std::uint8_t
        {
            const auto greenCount = std::ranges::count(guess, GuessColor::GREEN);
            const auto yellowCount = std::ranges::count(guess, GuessColor::YELLOW);
            switch (const auto sumCount = greenCount + yellowCount)
            {
            case 0:
                return 0;
            case 1:
                if (greenCount == 1)
                {
                    assert(yellowCount == 0);
                    static constexpr auto offset = details::offsets[0];
                    return offset + std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::GREEN));
                }
                // else
                {
                    assert(greenCount == 0);
                    assert(yellowCount == 1);
                    if (grayCount == 1)
                    {
                        static constexpr auto offset = details::offsets[1];
                        const auto yellowIndex = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::YELLOW));
                        const auto grayIndex = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::GRAY));
                        return offset + details::map(yellowIndex, grayIndex);
                    }
                    assert(grayCount == 2);
                    static constexpr auto offset = details::offsets[2];
                    const auto i = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::OTHER));
                    assert(i < 4);
                    const auto j = std::distance(std::ranges::begin(guess), std::ranges::find(guess | std::views::drop(i + 1), GuessColor::OTHER));
                    assert(j < 5);
                    return offset + details::map(i, j);
                }
            case 2:
                if (greenCount == 2)
                {
                    assert(yellowCount == 0);
                    static constexpr auto offset = details::offsets[3];
                    const auto i = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::GREEN));
                    assert(i < 4);
                    const auto j = std::distance(std::ranges::begin(guess), std::ranges::find(guess | std::views::drop(i + 1), GuessColor::GREEN));
                    assert(j < 5);
                    return offset + details::map(i, j);
                }
                else if (greenCount == 1)
                {
                    assert(yellowCount == 1);
                    assert(grayCount == 1);
                    static constexpr auto offset = details::offsets[4];
                    const auto i = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::YELLOW));
                    assert(i < 5);
                    const auto j = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::GRAY));
                    assert(j < 5);
                    assert(i != j);
                    const auto k = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::GREEN));
                    assert(k < 5);
                    assert(i != k);
                    assert(j != k);
                    return offset + details::map(i, j) * 3 + k - (i < k) - (j < k);
                }
            case 3:
            {
                assert(greenCount == 3);
                assert(yellowCount == 0);
                static constexpr auto offset = details::offsets[5];
                constexpr auto notGreen = [](auto c)
                { return c != GuessColor::GREEN; };
                const auto i = std::distance(std::ranges::begin(guess), std::ranges::find_if(guess, notGreen));
                const auto j = std::distance(std::ranges::begin(guess), std::ranges::find_if(guess | std::views::drop(i + 1), notGreen));
                return offset + details::map(i, j);
            }
            case 4:
            {
                assert(greenCount == 4);
                assert(yellowCount == 0);
                assert(grayCount == 1);
                static constexpr auto offset = details::offsets[6];
                constexpr auto notGreen = [](auto c)
                { return c != GuessColor::GREEN; };
                const auto i = std::distance(std::ranges::begin(guess), std::ranges::find_if(guess, notGreen));
                return offset + i;
            }
            case 5:
            {
                assert(greenCount == 5);
                assert(yellowCount == 0);
                assert(grayCount == 0);
                static constexpr auto offset = details::offsets[7];
                return offset;
            }
                // TODO default case
            }
        }();
    }
    else
    { // we only know the lower bound
        t = [guess]() -> std::uint8_t
        {
            const auto greenCount = std::ranges::count(guess, GuessColor::GREEN);
            const auto yellowCount = std::ranges::count(guess, GuessColor::YELLOW);
            switch (const auto sumCount = greenCount + yellowCount)
            {
            case 1:
                if (greenCount == 1)
                {
                    assert(yellowCount == 0);
                    static constexpr auto offset = details::offsets[8];
                    return offset + std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::GREEN));
                }
                // else
                assert(greenCount == 0);
                assert(yellowCount == 1);
                static constexpr auto offset = details::offsets[9];
                return offset + std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::YELLOW));
            case 2:
                if (greenCount == 2)
                {
                    assert(yellowCount == 0);
                    static constexpr auto offset = details::offsets[10];
                    const auto i = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::GREEN));
                    assert(i < 4);
                    const auto j = std::distance(std::ranges::begin(guess), std::ranges::find(guess | std::views::drop(i + 1), GuessColor::GREEN));
                    assert(j < 5);
                    return offset + details::map(i, j);
                }
                else if (yellowCount == 2)
                {
                    assert(greenCount == 0);
                    static constexpr auto offset = details::offsets[11];
                    const auto i = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::YELLOW));
                    assert(i < 4);
                    const auto j = std::distance(std::ranges::begin(guess), std::ranges::find(guess | std::views::drop(i + 1), GuessColor::YELLOW));
                    assert(j < 5);
                    return offset + details::map(i, j);
                }
                // else
                {
                    assert(greenCount == 1);
                    assert(yellowCount == 1);
                    static constexpr auto offset = details::offsets[12];
                    const auto i = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::GREEN));
                    assert(i < 5);
                    const auto j = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::YELLOW));
                    assert(j < 5);
                    assert(i != j);
                    return offset + i * 4 + j - static_cast<std::uint8_t>(i < j);
                }
            case 3:
                if (greenCount == 3)
                {
                    assert(yellowCount == 0);
                    static constexpr auto offset = details::offsets[13];
                    const auto i = std::distance(std::ranges::begin(guess), std::ranges::find_if(guess, notGreen));
                    assert(i < 4);
                    const auto j = std::distance(std::ranges::begin(guess), std::ranges::find_if(guess | std::views::drop(i + 1), notGreen));
                    assert(j < 5);
                    return offset + details::map(i, j);
                }
                // else
                {
                    assert(greenCount == 2);
                    assert(yellowCount == 1);
                    static constexpr auto offset = details::offsets[14];
                    const auto i = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::GREEN));
                    assert(i < 4);
                    const auto j = std::distance(std::ranges::begin(guess), std::ranges::find(guess | std::views::drop(i + 1), GuessColor::GREEN));
                    assert(j < 5);
                    const auto k = std::distance(std::ranges::begin(guess), std::ranges::find(guess, GuessColor::YELLOW));
                    assert(k < 5);
                    return details::map(i, j) * 3 + k - static_cast<std::uint8_t>(i < k) - static_cast<std::uint8_t>(j < k);
                }
            case 4:
            {
                assert(greenCount == 4);
                assert(yellowCount == 0);
                static constexpr auto offset = details::offsets[15];
                const auto i = std::distance(std::ranges::begin(guess), std::ranges::find_if(guess, notGreen));
                assert(i < 5);
                return offset + i;
            }
            case 5:
                return details::offsets[16];
            default:
                // letter was not in the guess
                return details::offsets[17];
            }
        }();
    }
}

std::array<Constraint<5>::GuessColor, 5> normalized(std::array<Constraint<5>::GuessColor, 5> guess)
{
    if (const auto yellowCount = std::ranges::count(guess, Constraint<5>::GuessColor::YELLOW); yellowCount == 1)
    {
        if (const auto greenCount = std::ranges::count(guess, Constraint<5>::GuessColor::GREEN); greenCount == 0)
        {
            if (const auto grayCount = std::ranges::count(guess, Constraint<5>::GuessColor::GRAY); grayCount == 3)
            {
                // Y X X X
                std::ranges::transform(guess, begin(guess), [](auto c)
                                       {
                    switch (c) {
                    case Constraint<5>::GuessColor::YELLOW:
                        return Constraint<5>::GuessColor::GRAY;
                    case Constraint<5>::GuessColor::OTHER:
                        return Constraint<5>::GuessColor::GREEN;
                    default:
                        return c;
                    } });
            }
        }
        else if (greenCount == 1)
        {
            if (const auto grayCount = std::ranges::count(guess, Constraint<5>::GuessColor::GRAY); grayCount == 2)
            {
                // G Y X X
                const auto OTHERIt = std::ranges::find(guess, Constraint<5>::GuessColor::OTHER);
                assert(OTHERIt != std::ranges::end(guess));
                const auto yellowIt = std::ranges::find(guess, Constraint<5>::GuessColor::YELLOW);
                assert(yellowIt != std::ranges::end(guess));
                *yellowIt = Constraint<5>::GuessColor::GRAY;
                *OTHERIt = Constraint<5>::GuessColor::GREEN;
            }
        }
        else if (greenCount == 2)
        {
            if (const auto grayCount = std::ranges::count(guess, Constraint<5>::GuessColor::GRAY); grayCount == 1)
            {
                // G G Y X
                std::ranges::transform(guess, begin(guess), [](auto c)
                                       {
                    switch (c) {
                    case Constraint<5>::GuessColor::YELLOW:
                        return Constraint<5>::GuessColor::GRAY;
                    case Constraint<5>::GuessColor::OTHER:
                        return Constraint<5>::GuessColor::GREEN;
                    default:
                        return c;
                    } });
            }
        }
        else if (greenCount == 3)
        {
            assert(std::ranges::count(guess, Constraint<5>::GuessColor::GRAY) == 0);
            // G G G Y
            std::ranges::transform(guess, begin(guess), [](auto c)
                                   { switch (c) { 
                                    case Constraint<5>::GuessColor::YELLOW: 
                                        return Constraint<5>::GuessColor::GRAY; 
                                    case Constraint<5>::GuessColor::OTHER: 
                                        return Constraint<5>::GuessColor::GREEN; 
                                    default: 
                                        return c; 
                                    } });
        }
    }
    else if (yellowCount == 2)
    {
        static constexpr auto isGreenOrGray = [](Constraint<5>::GuessColor c)
        { return c == Constraint<5>::GuessColor::GREEN or c == Constraint<5>::GuessColor::GRAY; };
        if (const auto greenGrayCount = std::ranges::count_if(guess, isGreenOrGray); greenGrayCount == 1)
        {
            // G Y Y / Y Y X
            static constexpr auto remap = [](Constraint<5>::GuessColor c)
            {
                switch (c)
                {
                case Constraint<5>::GuessColor::OTHER:
                    return Constraint<5>::GuessColor::GREEN;
                case Constraint<5>::GuessColor::YELLOW:
                    return Constraint<5>::GuessColor::GRAY;
                default:
                    return c;
                }
            };
            std::ranges::transform(guess, begin(guess), remap);
        }
    }
    return guess;
}

template <typename Allocator>
std::vector<Constraint<5>, Allocator> Constraint<5>::fromGuess(Wordle<5>::View guess, std::array<ResultColor, 5> reply)
{
    // TODO
    auto perChar = std::array<std::array<GuessColor, 5>, 26>{};
    static constexpr auto allGray = std::array{GuessColor::GRAY, GuessColor::GRAY, GuessColor::GRAY, GuessColor::GRAY, GuessColor::GRAY};
    std::ranges::fill(perChar, allGray);
    for (auto i = 0; i != N; ++i)
    {
        perChar[guess[i] - 'a'] = reply[i];
    }
    auto result = std::vector<Constraint<N>>{};
    for (auto i = 0; i != 26; ++i)
    {
        if (perChar[i] != allGray)
        {
            result.emplace_back(static_cast<char>('a' + i), normalized(perChar[i]));
        }
    }
    return result;
}

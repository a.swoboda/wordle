#pragma once

#include <array>
#include <iostream>

template <size_t N>
struct Wordle {
    using View = std::array<char, N>;

    Wordle() = default;
    Wordle(View v) : data(v) {}

    operator View() const noexcept { return data; }
    View view() const noexcept { return data; }

    template <std::integral I>
    [[nodiscard]] char operator[](I i) const { return data[i]; }

    friend std::istream& operator>>(std::istream& stream, Wordle<N>& wordle) {
        return stream.read(wordle.data.data(), N);
    }
    friend std::ostream& operator<<(std::ostream& stream, const Wordle<N>& wordle) {
        return stream.write(wordle.data.data(), N);
    }

private:
    std::array<char, N> data;
};
